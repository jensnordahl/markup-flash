#!/usr/bin/env python3
#
# Quick and dirty scripts for asking flash card style questions based on
# notes in an emacs .org file
#
# Rules
# - Org headings (starting with *) marks a section.
# - Lines starting with / contain questions
# - Following lines make up an answer
# - Line starting with . marks end of answer (optional)

import os
import sys
import random

class Question:

    def __init__(self, sectionName, question, answer):
        self.sectionName = sectionName
        self.question = question
        self.answer = answer
    

"""Parse an org mode file (given as a list of lines)
into questions. Returns a list of questions.
"""
def parseOrgForQuestions(lines):
    section=""
    question=""
    answer=""
    result = []
    
    for line in lines:
        if ( len(answer) > 0
             and len(question) > 0
             and ( line.startswith('*')
                   or line.startswith('/')
                   or line.startswith('.') )):
            result.append(Question(section, question, answer))
            answer = ""
            question = ""                                          
        if line.startswith('*'):
            section = line.lstrip('*').lstrip(' ').rstrip()
        elif line.startswith('/'):
            question = line.lstrip('/').rstrip().rstrip('/')
        elif (not line.startswith('.')
              and len(question) > 0
              and len(line.strip()) > 0 ):
            answer = answer + line

    if len(answer) > 0:
        result.append(Question(section, question, answer))

    return result

def askQuestions(questions):
    activeQuestions = list(questions)
    askAgainQuestions = []
    question = None
    
    while True:
        os.system('clear') # Clear terminal display
        question = pickNextQuestion(activeQuestions, askAgainQuestions, question)
        if question == None:
            break;
        
        sys.stdout.write("-------------------------------[%s (%s) / %s]---\n"
                         % (len(activeQuestions), len(askAgainQuestions), len(questions)))
        sys.stdout.write('(' + question.sectionName + ')\n')
        sys.stdout.write(question.question + '\n')
        sys.stdout.flush()
        sys.stdin.readline()
        sys.stdout.write(question.answer + '\n')
        sys.stdout.write("[s: mark as solved, a: ask again soon]")
        sys.stdout.flush()
        action = sys.stdin.readline()
        if (action.strip() == 's'):
            if question in activeQuestions: activeQuestions.remove(question)
            if question in askAgainQuestions: askAgainQuestions.remove(question)
        elif (action.strip() == 'a' and not question in askAgainQuestions):
            askAgainQuestions.append(question)

    sys.stdout.write("All solved!")

def pickNextQuestion(activeQuestions, askAgainQuestions, lastQuestion):
    aaqWithoutLast = list(set(askAgainQuestions) - set([lastQuestion]))
    aqWithoutLast = list(set(activeQuestions) - set([lastQuestion]))
    
    if (len(aaqWithoutLast) > 0 and random.randint(0,1) == 1):
        return random.choice(aaqWithoutLast)

    if (len(aqWithoutLast) > 0):
        return random.choice(aqWithoutLast)

    if (len(activeQuestions) > 0):
        return random.choice(activeQuestions)

    return None

def filterQuestions(questions, filterString):
    lowerFilter = filterString.lower()
    return [ q for q in questions if ( lowerFilter in q.sectionName.lower()
                                       or lowerFilter in q.question.lower() ) ]
            
def main():
    with open(sys.argv[1]) as orgFile:
        lines = orgFile.readlines()
    questions = parseOrgForQuestions(lines)
    if len(sys.argv) > 2:
        questions = filterQuestions(questions, sys.argv[2])
    askQuestions(questions)
    # for q in questions:
    #     print("---")
    #     print(q.sectionName)
    #     print(q.question)
    #     print(q.answer)
    
if __name__ == "__main__":
    try:
        main()
    except KeyboardInterrupt:
        pass
